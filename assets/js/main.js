$(document).ready(function() {
  $("#errorUsername").hide();
  $("#errorPassword").hide();

  $("#login").submit(function() { return false; });
  $("#submitLogin").on("click", function(){
    var username = $("#username").val();
    var password = $("#password").val();

    if(username == false){
      $("#errorUsername").show();
    }else{
      $("#errorUsername").hide();
    }

    if(password == false){
      $("#errorPassword").show();        
    }else{
      $("#errorPassword").hide();
    }

    if (username && password) {
      window.location.href = "admin/dashboard.html";
    }

  });
});

/*MENU*/
$(document).ready(function() {
  var
  burgerMenu = $(".sctd-burger"),
  burgerMenuContent = $(".sctd-burger-menu"),
  mainContent = $("#main-content");

  //Ketika mainContent di click menu akan menutup
  /*mainContent.click(function(){
    menu.removeClass('menu-open');
    burgerMenu.removeClass('burger-menu-open');
    $("body").removeClass("body-push-left");
  });*/

  //Ketika burgerMenu di click menu akan muncul
  burgerMenu.click(function(){
    burgerMenu.toggleClass('sctd-burger-open');
    burgerMenuContent.toggleClass('sctd-burger-menu-open');
  });


});
/*MENU*/

/*SECURITY*/
$(document).ready(function() {
  var
  sctdSeIconBox1 = $("#sctd-se-icon-box-1"), sctdSeIconBox2 = $("#sctd-se-icon-box-2"),
  sctdSeContentBox1 = $("#sctd-se-content-box-1"), sctdSeContentBox2 = $("#sctd-se-content-box-2");

  sctdSeContentBox2.hide();

  //Ketika sctdSeIconBox1 di click sctdSeContentBox1 akan muncul
  sctdSeIconBox1.click(function(){
    sctdSeIconBox1.addClass('sctd-se-icon-box-active');
    sctdSeIconBox2.removeClass('sctd-se-icon-box-active');
    
    sctdSeContentBox1.show();
    sctdSeContentBox2.hide();
  });

  //Ketika sctdSeIconBox2 di click sctdSeContentBox2 akan muncul
  sctdSeIconBox2.click(function(){
    sctdSeIconBox2.addClass('sctd-se-icon-box-active');
    sctdSeIconBox1.removeClass('sctd-se-icon-box-active');
    
    sctdSeContentBox2.show();
    sctdSeContentBox1.hide();
  });


});
/*SECURITY*/

/*ADVANCE DATA*/
$(document).ready(function() {
  var
  sctdSeIconBox1 = $("#sctd-ad-icon-box-1"), sctdSeIconBox2 = $("#sctd-ad-icon-box-2"),
  sctdSeContentBox1 = $("#sctd-ad-content-box-1"), sctdSeContentBox2 = $("#sctd-ad-content-box-2");

  sctdSeContentBox2.hide();

  //Ketika sctdSeIconBox1 di click sctdSeContentBox1 akan muncul
  sctdSeIconBox1.click(function(){
    sctdSeIconBox1.addClass('sctd-se-icon-box-active');
    sctdSeIconBox2.removeClass('sctd-se-icon-box-active');
    
    sctdSeContentBox1.show();
    sctdSeContentBox2.hide();
  });

  //Ketika sctdSeIconBox2 di click sctdSeContentBox2 akan muncul
  sctdSeIconBox2.click(function(){
    sctdSeIconBox2.addClass('sctd-se-icon-box-active');
    sctdSeIconBox1.removeClass('sctd-se-icon-box-active');
    
    sctdSeContentBox2.show();
    sctdSeContentBox1.hide();
  });


});
/*ADVANCE DATA*/

/*TIME*/
(function($){$.clock={version:"2.0.2",locale:{}};t=[];$.fn.clock=function(d){var c={it:{weekdays:["Domenica","Lunedì","Martedì","Mercoledì","Giovedì","Venerdì","Sabato"],months:["Gennaio","Febbraio","Marzo","Aprile","Maggio","Giugno","Luglio","Agosto","Settembre","Ottobre","Novembre","Dicembre"]},en:{weekdays:["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"],months:["January","February","March","April","May","June","July","August","September","October","November","December"]},es:{weekdays:["Domingo","Lunes","Martes","Miércoles","Jueves","Viernes","Sábado"],months:["Enero","Febrero","Marzo","Abril","May","junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]},de:{weekdays:["Sonntag","Montag","Dienstag","Mittwoch","Donnerstag","Freitag","Samstag"],months:["Januar","Februar","März","April","könnte","Juni","Juli","August","September","Oktober","November","Dezember"]},fr:{weekdays:["Dimanche","Lundi","Mardi","Mercredi","Jeudi","Vendredi","Samedi"],months:["Janvier","Février","Mars","Avril","May","Juin","Juillet","Août","Septembre","Octobre","Novembre","Décembre"]},ru:{weekdays:["Воскресенье","Понедельник","Вторник","Среда","Четверг","Пятница","Суббота"],months:["Январь","Февраль","Март","Апрель","Май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"]}};return this.each(function(){$.extend(c,$.clock.locale);d=d||{};d.timestamp=d.timestamp||"z";y=new Date().getTime();d.sysdiff=0;if(d.timestamp!="z"){d.sysdiff=d.timestamp-y}d.langSet=d.langSet||"en";d.format=d.format||((d.langSet!="en")?"24":"12");d.calendar=d.calendar||"true";d.seconds=d.seconds||"true";if(!$(this).hasClass("jqclock")){$(this).addClass("jqclock")}var e=function(g){if(g<10){g="0"+g}return g},f=function(j,n){var r=$(j).attr("id");if(n=="destroy"){clearTimeout(t[r])}else{m=new Date(new Date().getTime()+n.sysdiff);var p=m.getHours(),l=m.getMinutes(),v=m.getSeconds(),u=m.getDay(),i=m.getDate(),k=m.getMonth(),q=m.getFullYear(),o="",z="",w=n.langSet;if(n.format=="12"){o=" AM";if(p>11){o=" PM"}if(p>12){p=p-12}if(p==0){p=12}}p=e(p);l=e(l);v=e(v);if(n.calendar!="false"){z=((w=="en")?"<span class='clockdate'>"+c[w].weekdays[u]+", "+c[w].months[k]+" "+i+", "+q+"</span>":"<span class='clockdate'>"+c[w].weekdays[u]+", "+i+" "+c[w].months[k]+" "+q+"</span>")}$(j).html(z+"<span class='clocktime'>"+p+":"+l+(n.seconds=="true"?":"+v:"")+o+"</span>");t[r]=setTimeout(function(){f($(j),n)},1000)}};f($(this),d)})};return this})(jQuery);
$(document).ready(function(){
  $("#clock1").clock();                                       
});   
/*TIME*/

/*MAP*/
jQuery('#vmap').vectorMap({
    map: 'world_en',
    backgroundColor: '#ffffff',
    borderColor: '#ffffff',
    borderOpacity: 0.25,
    borderWidth: 2,
    color: '#32485F',
    enableZoom: true,
    showTooltip: true,
    hoverColor: '#5FAE91',
    hoverOpacity: null,
    normalizeFunction: 'polynomial',
    scaleColors: ['#b6d6ff', '#005ace'],
    selectedColor: '#5FAE91',
    selectedRegions: null,
    onRegionClick: function(element, code, region)
    {
        var message = region
            + ' IP Detected : ';

        //alert(message);
        $('#sctd-rms-country').text(message);
    }
});
/*MAP*/

/*DATATABLES*/
$(document).ready(function() {
  $('#datatable').DataTable({
    "lengthMenu": [[10, 25, 50, -1], [10, 25, 50, "All"]],
    "scrollY":        "250px",
    "scrollCollapse": true
  });
});
jQuery('#datatable .group-checkable').change(function () {
    var set = jQuery(this).attr("data-set");
    var checked = jQuery(this).is(":checked");
    jQuery(set).each(function () {
        if (checked) {
            $(this).attr("checked", true);
        } else {
            $(this).attr("checked", false);
        }
        $(this).parents('tr').toggleClass("active");
    });
    jQuery.uniform.update(set);
});

jQuery('#datatable tbody tr .checkboxes').change(function(){
     $(this).parents('tr').toggleClass("active");
});
/*DATATABLES*/

$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});